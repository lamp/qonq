# qonq

Simple file host that puts the file code in the hostname. The leftmost part of the Host (i.e. `1234` of `1234.qonq.gq`) is used to serve a corresponding folder (i.e. `files/1234/`); if the folder contains one file then it is served directly, but if it contains multiple files then it is served as a webroot with a directory index. Designed for use as a ShareX Custom Uploader, but you can drop any lowercase alphanumeric folder into the files directory to serve it instantly.

You can also put the file code in the URL of the main domain. For example, https://1234.qonq.gq/ and https://qonq.gq/1234/ are the same. If there is no code in either the URL or subdomain, then the default code is `www`, which means https://www.qonq.gq/ and https://qonq.gq/ are the same, and you can put content in `files/www` for it (`index.html` etc).

## Usage
Clone repository and install dependencies with `npm ci`.

The following environment variables are required for operation. You can put them in an `.env` file, which the program will load itself.

| Variable | Description |
|----------|-------------|
| `AUTH_TOKEN` | Unique confidential key to authenticate uploads |

The following environment variables are optional.

| Variable | Description | Default |
|----------|-------------|---------|
| `BASE_HOSTNAME` | Overrides the hostname for generating URLs. | host header on the upload request |
| `DISCORD_WEBHOOK` | Discord webhook url that newly-uploaded URLs will be sent to to pre-load their embed, which theoretically makes them embed again faster. | `undefined` (disabled) |
| `PORT` | TCP port to listen on | `8568` |
| `ADDRESS` | Address to bind to | `'0.0.0.0'` (all) |
| `FILES_DIR` | Directory to store the files in | `'files'` (relative of working directory) |
| `TRUST_PROXY` | Value for express's [`'trust proxy'`](https://expressjs.com/en/5x/api.html#trust.proxy.options.table) setting | `'loopback'` |


Run with `node qonq.js` or your favorite init system.


## ShareX config
```
{
  "Version": "12.4.1",
  "DestinationType": "ImageUploader, TextUploader, FileUploader",
  "RequestMethod": "POST",
  "RequestURL": "https://qonq.gq/",
  "Headers": {
    "authentication": "paste value of AUTH_TOKEN here",
    "filename": "$filename$"
  },
  "Body": "Binary"
}
```
You can import this custom uploader config and modify the Request URL and authentication fields as necessary.


## Nginx config example
```
server {
        server_name qonq.gq *.qonq.gq;
        listen 443 ssl;
        ssl_certificate /etc/letsencrypt/live/qonq.gq/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/qonq.gq/privkey.pem;
        include /etc/letsencrypt/options-ssl-nginx.conf;
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
        client_max_body_size 1G;
        location / {
                proxy_set_header X-Forwarded-Host $host;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Proto $scheme;
                proxy_pass http://localhost:8568;
        }
}
server {
        server_name qonq.gq *.qonq.gq;
        listen 80;
        return 308 https://$host$request_uri;
}
```


## Command lines for Mac

There is no ShareX for Mac so here are some command lines you can use with [iCanHazShortcut](https://github.com/deseven/icanhazshortcut)

```bash
# for fullscreen
SS=${TMPDIR}screenshot.png; screencapture -Cdt jpg $SS && URL=$(curl -H "Authentication: your_auth_token" -F "file=@$SS" https://qonq.gq/upload) && tr -d '\n' <<< $URL | pbcopy && osascript -e "display notification \"$URL\" with title \"Screenshot upload complete\"" && rm $SS

# for interactive
SS=${TMPDIR}screenshot.png; screencapture -idt png $SS && URL=$(curl -H "Authentication: your_auth_token" -F "file=@$SS" https://qonq.gq/upload) && tr -d '\n' <<< $URL | pbcopy && osascript -e "display notification \"$URL\" with title \"Screenshot upload complete\"" && rm $SS
```
