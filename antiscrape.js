// someone could iterate over all ~1.6 million possible file codes to download all files.
// prevent this by banning IP addresses that request too many non-existant files.

var ip404 = {};
module.exports = (req, res, next) => {
	if (ip404[req.ip]?.size > 10)
		return res.status(403).send("Banned");
	res.on("finish", () => {
		if (res.statusCode == 404 && req.filecode) {
			if (!ip404[req.ip]) ip404[req.ip] = new Set();
			ip404[req.ip].add(req.filecode);
		}
	});
	next();
};